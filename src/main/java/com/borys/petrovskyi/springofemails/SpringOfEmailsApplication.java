package com.borys.petrovskyi.springofemails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringOfEmailsApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringOfEmailsApplication.class, args);
	}
}
