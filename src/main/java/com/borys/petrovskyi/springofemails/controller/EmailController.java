package com.borys.petrovskyi.springofemails.controller;

import com.borys.petrovskyi.springofemails.controller.request.EmailInfoCreateRequest;
import com.borys.petrovskyi.springofemails.controller.request.EmailInfoUpdateRequest;
import com.borys.petrovskyi.springofemails.controller.response.EmailReport.EmailInfo;
import com.borys.petrovskyi.springofemails.service.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class EmailController {
    private static final String EMAILS = "/emails";
    private final EmailService emailService;

    @GetMapping(value = EMAILS + "/{email}",
            produces = {MediaType.APPLICATION_XML_VALUE})
    public EmailInfo getInfoByEmail(@PathVariable("email") String email) {
        log.debug("Get email info by email=[{}]", email);
        return emailService.getByEmail(email);
    }

    @PostMapping(value = EMAILS,
            consumes = {MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE})
    public EmailInfo createChartBuilder(@RequestBody EmailInfoCreateRequest request) {
        log.debug("Create email info by request=[{}]", request);
        return emailService.createEmailInfo(request);
    }

    @PutMapping(value = EMAILS + "/{email}",
            consumes = {MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE})
    public EmailInfo editChartBuilder(@PathVariable("email") String email, @RequestBody EmailInfoUpdateRequest request) {
        log.debug("Update email info by email=[{}] with request=[{}]", email, request);
        return emailService.updateEmailInfo(email, request);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @DeleteMapping(value = EMAILS + "/{email}")
    public void deleteChartBuilder(@PathVariable("email") String email) {
        log.debug("Delete email info by email=[{}]", email);
        emailService.deleteEmailInto(email);
    }

}
