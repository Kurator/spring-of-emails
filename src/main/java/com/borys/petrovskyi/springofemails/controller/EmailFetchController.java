package com.borys.petrovskyi.springofemails.controller;

import com.borys.petrovskyi.springofemails.controller.request.Dataset;
import com.borys.petrovskyi.springofemails.controller.response.EmailReport;
import com.borys.petrovskyi.springofemails.controller.response.EmailsCountResponse;
import com.borys.petrovskyi.springofemails.service.EmailFetchService;
import com.borys.petrovskyi.springofemails.validation.ValidateEmails;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequiredArgsConstructor
public class EmailFetchController {
    private static final String FETCH = "/fetch";
    private final EmailFetchService emailFetchService;

    @GetMapping(value = FETCH + "/count",
            produces = {MediaType.APPLICATION_XML_VALUE})
    public EmailsCountResponse getEmailsCount() {
        return emailFetchService.getEmailsCount();
    }

    @GetMapping(value = FETCH + "/report",
            produces = {MediaType.APPLICATION_XML_VALUE})
    public EmailReport getEmailsReport() {
        return emailFetchService.getEmailReport();
    }

    @PostMapping(value = FETCH,
            produces = {MediaType.APPLICATION_XML_VALUE},
            consumes = {MediaType.APPLICATION_XML_VALUE})
    public EmailReport fetchData(@ValidateEmails @RequestBody Dataset dataset) {
        return emailFetchService.fetchData(dataset);
    }

    @GetMapping(value = FETCH + "/getXML")
    public String getXml() {
        return "<Dataset><emails><email>bpet@comenon.com</email><email>bpet@comenon123.com</email><email>djaks@comenon.com</email></emails></Dataset>";
    }
}
