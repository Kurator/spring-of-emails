package com.borys.petrovskyi.springofemails.controller.exception;

import lombok.Getter;

@Getter
class DomainException extends RuntimeException {
    private final String internalErrorCode;

    DomainException (String internalErrorCode, String message) {
        super(message);
        this.internalErrorCode = internalErrorCode;
    }
}
