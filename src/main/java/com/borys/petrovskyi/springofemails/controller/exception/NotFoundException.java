package com.borys.petrovskyi.springofemails.controller.exception;

public class NotFoundException extends DomainException {
    private static final String DEFAULT_MESSAGE = "Not found";

    public NotFoundException(String customMessage) {
        super(DEFAULT_MESSAGE, customMessage);
    }
}
