package com.borys.petrovskyi.springofemails.controller.request;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Dataset {
    private List<String> emails;
    private List<String> resources;
}
