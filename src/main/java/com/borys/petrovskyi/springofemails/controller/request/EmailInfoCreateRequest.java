package com.borys.petrovskyi.springofemails.controller.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailInfoCreateRequest {
    @NotBlank
    private String email;
    @NotNull
    private Long count;
}
