package com.borys.petrovskyi.springofemails.controller.request;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmailInfoUpdateRequest {
    @NotNull
    private Long count;
}
