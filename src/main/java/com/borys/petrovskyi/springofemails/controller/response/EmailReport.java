package com.borys.petrovskyi.springofemails.controller.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@JacksonXmlRootElement
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class EmailReport {
    private final String uuid;
    @JacksonXmlProperty(localName = "emailInfo")
    @JacksonXmlElementWrapper(useWrapping = false)
    private final List<EmailInfo> emailInfoList;

    @Getter
    @AllArgsConstructor
    public static class EmailInfo {
        private final String email;
        private final Long count;
    }
}
