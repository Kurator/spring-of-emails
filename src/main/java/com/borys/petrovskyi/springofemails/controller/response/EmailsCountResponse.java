package com.borys.petrovskyi.springofemails.controller.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class EmailsCountResponse {
    private final Long emailsCount;
}
