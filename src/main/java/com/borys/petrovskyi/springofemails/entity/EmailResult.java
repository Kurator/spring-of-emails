package com.borys.petrovskyi.springofemails.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table( name = "email_results")
@NoArgsConstructor
@AllArgsConstructor
public class EmailResult {
    @Id
    @Column( name ="email", updatable = false, nullable = false)
    private String email;
    @Column( name = "count")
    private Long count;
    @OneToMany(mappedBy="parent", fetch = FetchType.LAZY)
    private List<Report> children;
}
