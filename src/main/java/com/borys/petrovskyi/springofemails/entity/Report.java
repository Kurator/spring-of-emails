package com.borys.petrovskyi.springofemails.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table( name = "report")
@NoArgsConstructor
public class Report {
    @EmbeddedId
    private ReportId id;
    @Column(name = "count")
    private Long count;
    @ManyToOne
    @JoinColumn(name = "email", insertable = false, updatable = false)
    private EmailResult parent;

    public Report(String uuid, String email, Long count) {
        this.id = new ReportId(uuid, email);
        this.count = count;
    }
}
