package com.borys.petrovskyi.springofemails.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class ReportId implements Serializable {
    @Column(name = "uuid")
    private String uuid;
    @Column(name = "email")
    private String email;
}
