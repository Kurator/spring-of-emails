package com.borys.petrovskyi.springofemails.repository;

import com.borys.petrovskyi.springofemails.entity.EmailResult;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<EmailResult, String> {
    List<EmailResult> findAllByEmailIn(Collection<String> emails);
    default Map<String, EmailResult> findAllByEmailsMap(Collection<String> emails) {
        return findAllByEmailIn(emails).stream().collect(Collectors.toMap(EmailResult::getEmail, v -> v));
    }
}
