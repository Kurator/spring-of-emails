package com.borys.petrovskyi.springofemails.repository;

import com.borys.petrovskyi.springofemails.entity.Report;
import com.borys.petrovskyi.springofemails.entity.ReportId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<Report, ReportId> {}
