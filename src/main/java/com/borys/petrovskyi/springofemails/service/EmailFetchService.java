package com.borys.petrovskyi.springofemails.service;

import com.borys.petrovskyi.springofemails.controller.request.Dataset;
import com.borys.petrovskyi.springofemails.controller.response.EmailReport;
import com.borys.petrovskyi.springofemails.controller.response.EmailsCountResponse;

public interface EmailFetchService {
    EmailsCountResponse getEmailsCount();
    EmailReport getEmailReport();
    EmailReport fetchData(Dataset dataset);
}
