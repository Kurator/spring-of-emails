package com.borys.petrovskyi.springofemails.service;

import com.borys.petrovskyi.springofemails.controller.request.EmailInfoCreateRequest;
import com.borys.petrovskyi.springofemails.controller.request.EmailInfoUpdateRequest;
import com.borys.petrovskyi.springofemails.controller.response.EmailReport.EmailInfo;

public interface EmailService {
    EmailInfo getByEmail(String email);
    EmailInfo createEmailInfo(EmailInfoCreateRequest request);
    EmailInfo updateEmailInfo(String email, EmailInfoUpdateRequest request);
    void deleteEmailInto(String email);
}
