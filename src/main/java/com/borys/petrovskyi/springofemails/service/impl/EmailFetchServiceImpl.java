package com.borys.petrovskyi.springofemails.service.impl;

import com.borys.petrovskyi.springofemails.controller.request.Dataset;
import com.borys.petrovskyi.springofemails.controller.response.EmailReport;
import com.borys.petrovskyi.springofemails.controller.response.EmailReport.EmailInfo;
import com.borys.petrovskyi.springofemails.controller.response.EmailsCountResponse;
import com.borys.petrovskyi.springofemails.entity.EmailResult;
import com.borys.petrovskyi.springofemails.entity.Report;
import com.borys.petrovskyi.springofemails.repository.EmailRepository;
import com.borys.petrovskyi.springofemails.repository.ReportRepository;
import com.borys.petrovskyi.springofemails.service.EmailFetchService;
import com.borys.petrovskyi.springofemails.utils.FutureCounterTask;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailFetchServiceImpl implements EmailFetchService {

    @Value("${thread.count}")
    private Integer threadCounts;
    @Value("${thread.timeout_minutes}")
    private Integer timeoutMinutes;
    private final EmailRepository emailRepository;
    private final ReportRepository reportRepository;
    private ExecutorService executorService;

    @PostConstruct
    public void init() {
        executorService = Executors.newFixedThreadPool(threadCounts != null ? threadCounts: 1);
    }

    @Override
    public EmailsCountResponse getEmailsCount() {
        return new EmailsCountResponse(emailRepository.count());
    }

    @Override
    public EmailReport getEmailReport() {
        return new EmailReport(null,
                emailRepository.findAll()
                        .stream()
                            .map(emailResult -> new EmailInfo(emailResult.getEmail(), emailResult.getCount()))
                                .collect(Collectors.toList()));
    }

    @Override
    public EmailReport fetchData(Dataset dataset) {
        Map<String, Long> emailOccurrence = new HashMap<>();
        executeTask(new FutureCounterTask(emailOccurrence, dataset));
        log.info("Saving collection of occurred emails with size=[{}]", emailOccurrence.size());
        mergeAndStoreEmails(emailOccurrence);
        final String uuid = UUID.randomUUID().toString();
        List<Report> runReport = mergeAndStoreReports(uuid, emailOccurrence);
        return new EmailReport(uuid,
                runReport.stream()
                    .map(emailResult -> new EmailInfo(emailResult.getId().getEmail(), emailResult.getCount()))
                        .collect(Collectors.toList()));
    }

    @Transactional
    List<Report> mergeAndStoreReports(String uuid, Map<String, Long> emailOccurrence) {
        List<Report> listToStore = new ArrayList<>();
        emailOccurrence.forEach((email, count) -> listToStore.add(new Report(uuid, email, count)));
        return reportRepository.saveAll(listToStore);
    }

    @Transactional
    void mergeAndStoreEmails(Map<String, Long> emailOccurrence) {
        Map<String, EmailResult> emailResultMap = emailRepository.findAllByEmailsMap(emailOccurrence.keySet());
        List<EmailResult> listToStore = new ArrayList<>();
        for (String email: emailOccurrence.keySet()) {
            EmailResult emailResult;
            if (emailResultMap.containsKey(email)) {
                emailResult = emailResultMap.get(email);
                emailResult.setCount(emailResult.getCount() + emailOccurrence.get(email));
            } else {
                emailResult = new EmailResult();
                emailResult.setEmail(email);
                emailResult.setCount(emailOccurrence.get(email));
            }
            listToStore.add(emailResult);
        }
        emailRepository.saveAll(listToStore);
    }

    public void executeTask(Callable<Void> c) {
        final Future<Void> future = executorService.submit(c);
        try {
            future.get(timeoutMinutes, TimeUnit.MINUTES);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            log.info("Reached execution timeout. Trying to save collection that is fulfilled for now.");
            log.debug("Stack trace of interruption exception:", e);
            future.cancel(true); //this method will stop the running underlying task
        }
    }
}
