package com.borys.petrovskyi.springofemails.service.impl;

import com.borys.petrovskyi.springofemails.controller.exception.NotFoundException;
import com.borys.petrovskyi.springofemails.controller.request.EmailInfoCreateRequest;
import com.borys.petrovskyi.springofemails.controller.request.EmailInfoUpdateRequest;
import com.borys.petrovskyi.springofemails.controller.response.EmailReport.EmailInfo;
import com.borys.petrovskyi.springofemails.entity.EmailResult;
import com.borys.petrovskyi.springofemails.repository.EmailRepository;
import com.borys.petrovskyi.springofemails.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {
    private final EmailRepository emailRepository;
    @Override
    public EmailInfo getByEmail(String email) {
        EmailResult emailResult = emailRepository.findById(email)
                .orElseThrow(() -> new NotFoundException("Cannot get email info by email " + email));
        return new EmailInfo(emailResult.getEmail(), emailResult.getCount());
    }

    @Override
    public EmailInfo createEmailInfo(EmailInfoCreateRequest request) {
        EmailResult emailResult = new EmailResult(request.getEmail(), request.getCount(), null);
        emailResult = emailRepository.save(emailResult);
        return new EmailInfo(emailResult.getEmail(), emailResult.getCount());
    }

    @Override
    public EmailInfo updateEmailInfo(String email, EmailInfoUpdateRequest request) {
        EmailResult emailResult = emailRepository.findById(email)
                .orElseThrow(() -> new NotFoundException("Cannot get email info by email " + email));
        emailResult.setCount(request.getCount());
        emailRepository.save(emailResult);
        return new EmailInfo(emailResult.getEmail(), emailResult.getCount());
    }

    @Override
    public void deleteEmailInto(String email) {
        EmailResult emailResult = emailRepository.findById(email)
                .orElseThrow(() -> new NotFoundException("Cannot get email info by email " + email));
        emailRepository.delete(emailResult);
    }
}
