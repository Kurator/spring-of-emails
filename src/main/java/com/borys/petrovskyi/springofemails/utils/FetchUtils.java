package com.borys.petrovskyi.springofemails.utils;

import com.borys.petrovskyi.springofemails.controller.request.Dataset;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class FetchUtils {
    private static final RestTemplate restTemplate = new RestTemplate();

    public static Dataset getDatasetFromUrl(String url) {
        String resourceResult;
        try {
            log.info("Attempt to reach dataset from remote server. Url=[{}]", url);
            resourceResult = restTemplate.getForObject(url, String.class);
        } catch (Exception e) {
            log.warn("Exception during resource fetch! ");
            return null;
        }
        Dataset dataset = XMLUtils.getDatasetFromXmlString(resourceResult);
        if (dataset != null && dataset.getEmails() != null) {
            log.info("Validation result for dataset from url=[{}] is [{}]", url, ValidationUtils.validateDataset(dataset));
        }
        return dataset;
    }
}
