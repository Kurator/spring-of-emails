package com.borys.petrovskyi.springofemails.utils;

import com.borys.petrovskyi.springofemails.controller.request.Dataset;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

public class FutureCounterTask implements Callable<Void>  {

    private final Map<String, Long> emailOccurrence;
    private final Dataset dataset;

    public FutureCounterTask(Map<String, Long> emailOccurrence, Dataset dataset) {
        this.dataset = dataset;
        this.emailOccurrence = emailOccurrence;
    }

    @Override
    public Void call() {
        Set<String> handledUrls = new HashSet<>();
        handleDataset(dataset, emailOccurrence, handledUrls);
        return null;
    }

    private void handleDataset(Dataset dataset, Map<String, Long> emailOccurrence, Set<String> handledUrls) {
        fillOccurrenceMap(emailOccurrence, dataset.getEmails());
        if (dataset.getResources() != null) {
            for (String url : dataset.getResources()) {
                if (!handledUrls.contains(url)) {
                    Dataset newDataset = FetchUtils.getDatasetFromUrl(url);
                    if (newDataset != null) {
                        handleDataset(newDataset, emailOccurrence, handledUrls);
                    }
                }
                handledUrls.add(url);
            }
        }
    }

    private synchronized void fillOccurrenceMap(Map<String, Long> occurrence, List<String> emails) {
        for (String email: emails) {
            occurrence.put(email, (occurrence.get(email) == null ? 0 : occurrence.get(email)) + 1);
        }
    }
}
