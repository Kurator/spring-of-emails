package com.borys.petrovskyi.springofemails.utils;

import com.borys.petrovskyi.springofemails.controller.request.Dataset;
import java.util.Arrays;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

@Slf4j
public class ValidationUtils {
    private static final Properties crmProperties;
    private static String[] whitelist;
    private static final Pattern pattern = Pattern.compile("^(.+)@(.+)$");
    static {
        crmProperties = new Properties();
        try {
            crmProperties.load(new ClassPathResource("application.properties").getInputStream());
            whitelist = crmProperties.getProperty("email.whitelist").split(",",-1);
            log.info("Whitelist has been generated=[{}]", Arrays.toString(whitelist));
        } catch (Exception e) {
            log.error("Error during whitelist initialization!", e);
        }
    }
    public static boolean validateDataset(Dataset dataset) {
        dataset.setEmails(dataset.getEmails().stream()
            .filter(email -> pattern.matcher(email).matches() && Arrays.stream(whitelist).anyMatch(email::endsWith))
                .collect(Collectors.toList()));
        return !dataset.getEmails().isEmpty();
    }
}
