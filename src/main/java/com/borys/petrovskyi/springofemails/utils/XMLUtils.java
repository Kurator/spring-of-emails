package com.borys.petrovskyi.springofemails.utils;

import com.borys.petrovskyi.springofemails.controller.request.Dataset;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XMLUtils {
    private static final XmlMapper xmlMapper = new XmlMapper();
    public static Dataset getDatasetFromXmlString(String xmlString) {
        try {
            return xmlMapper.readValue(xmlString, Dataset.class);
        } catch (IOException e) {
            log.warn("Error during parsing Dataset", e);
        }
        return null;
    }
}
