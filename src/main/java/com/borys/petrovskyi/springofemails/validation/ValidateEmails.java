package com.borys.petrovskyi.springofemails.validation;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import com.borys.petrovskyi.springofemails.validation.validator.EmailValidator;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Target({PARAMETER})
@Retention(RUNTIME)
@Constraint(validatedBy = EmailValidator.class)
@Documented
public @interface ValidateEmails {
    String message() default "No valid emails in this list!";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
