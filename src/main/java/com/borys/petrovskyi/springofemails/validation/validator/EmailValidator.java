package com.borys.petrovskyi.springofemails.validation.validator;

import com.borys.petrovskyi.springofemails.controller.request.Dataset;
import com.borys.petrovskyi.springofemails.utils.ValidationUtils;
import com.borys.petrovskyi.springofemails.validation.ValidateEmails;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailValidator implements ConstraintValidator<ValidateEmails, Dataset> {
    @Override
    public boolean isValid(Dataset dataset, ConstraintValidatorContext constraintValidatorContext) {
        // We can use parallel stream if we know, that list of emails will be huge enough every time
        return ValidationUtils.validateDataset(dataset);
    }
}
